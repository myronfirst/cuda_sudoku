IDIR=includes
ODIR=objects
CDIR=./
_DEPS= DFS.h ChildGrids.h Grid.h Utilities.h
_OBJ=DFS.o ChildGrids.o Grid.o Utilities.o main.o

DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

DEBUGFLAGS=
# DEBUGFLAGS=-DNDEBUG
WFLAGS=-Wall -Wextra -Wfloat-equal -Wshadow -Wpointer-arith\
-Wcast-align -Wcast-qual -Wstrict-overflow=5 -Wwrite-strings\
-Wswitch-default -Wswitch-enum -Wconversion -Wunreachable-code\
-Winit-self #-Wundef

CPP=g++
CPPFLAGS=-g3 -O0 -std=c++11 $(WFLAGS) $(DEBUGFLAGS) -I$(IDIR)
NVCC=nvcc
NVCCFLAGS=-g -G -O0 -m64 -gencode arch=compute_35,code=sm_35 -std=c++11 --compiler-options "$(WFLAGS)" $(DEBUGFLAGS) -I$(IDIR)

.PHONY: clean
default: main

$(ODIR)/%.o: $(CDIR)/%.cpp $(DEPS)
	@$(NVCC) -x cu -dc -o $@ $< $(NVCCFLAGS)

main: $(OBJ)
	@$(NVCC) -o $@ $^ $(NVCCFLAGS)

clean:
	rm -f $(ODIR)/*.o
	rm -f main
