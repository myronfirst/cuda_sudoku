#include "Utilities.h"

using namespace std;
inline void GpuAssert(cudaError_t code, const char *file, int line, bool abort) {
	if (code != cudaSuccess) {
		fprintf(stdout, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}

void *Managed::operator new(size_t len) {
	void *ptr;
	GPUAssert(cudaMallocManaged(&ptr, len));
	GPUAssert(cudaDeviceSynchronize());
	return ptr;
}

void Managed::operator delete(void *ptr) {
	GPUAssert(cudaDeviceSynchronize());
	GPUAssert(cudaFree(ptr));
}

unsigned long GetTimeMillis(void) {
	struct timeval tv;
	int ret = gettimeofday(&tv, NULL);
	if (ret == 0) return (1000 * tv.tv_sec) + (tv.tv_usec / 1000);
	cout << "GetTimeMillis error" << endl;
	exit(EXIT_FAILURE);
}
