#include "ChildGrids.h"
#include <cstdio>

using namespace std;
using GridArray = SovietArray<Grid *>;
__managed__ int childIndex = 0;
__device__ int cellCount = 0;
__global__ void threadChildGrids(int totalThreads, int maxCells, const Grid *grid, GridArray *childGrids);

void ComputeChildGrids(int blocks, int blockThreads, int maxCells, const Grid *grid, GridArray *childGrids) {
	if (maxCells == 0) {
		grid->Copy(childGrids->index(0));
		childGrids->size(1);
		return;
	}
	int totalThreads = blocks * blockThreads;
	childIndex = 0;
	threadChildGrids<<<blocks, blockThreads>>>(totalThreads, maxCells, grid, childGrids);
	GPUAssert(cudaDeviceSynchronize());
	if (childIndex < childGrids->size()) childGrids->size(childIndex);
}

__global__ void threadChildGrids(int totalThreads, int maxCells, const Grid *grid, GridArray *childGrids) {
	int threadIndex = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = (totalThreads < blockDim.x * gridDim.x) ? totalThreads : blockDim.x * gridDim.x;
	for (int index = threadIndex; index < grid->sizeTotal; index += stride) {
		int i, j;
		grid->GetRowCol(&i, &j, index);
		if (grid->Get(i, j) > 0) continue;
		if (cellCount >= maxCells || atomicAdd(&cellCount, 1) >= maxCells) return;
		bool *candidates = grid->GetCandidates(i, j);
		cudaDeviceSynchronize();
		for (int k = 1; k <= grid->sizeDim; ++k) {
			if (!candidates[k]) continue;
			int threadChildIndex = atomicAdd(&childIndex, 1);
			assert(threadChildIndex <= childGrids->size());
			grid->Copy(childGrids->index(threadChildIndex));
			childGrids->index(threadChildIndex)->Put(i, j, k);
		}
	}
}
