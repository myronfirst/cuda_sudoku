#include "DFS.h"

using GridArray = SovietArray<Grid *>;

__global__ void DFS(int totalThreads, GridArray *boards, Grid *solution);
__global__ void DFSMyron(int totalThreads, GridArray *children, Grid *solution);
__device__ bool Backtrack(Grid *const &grid, Grid *const &solution, const int &index);

__device__ int finished = 0;

__global__ void DFS(int totalThreads, GridArray *boards, Grid *solution) {
	int index = blockDim.x * blockIdx.x + threadIdx.x;
	int stride = (totalThreads < blockDim.x * gridDim.x) ? totalThreads : blockDim.x * gridDim.x;

	while (finished == 0 && (index < boards->size())) {
		Grid *myBoard = boards->index(index);

		myBoard->CreateEmptySpaces();
		myBoard->AllocateEmptySpacesCandidates();

		int numberOfEmptySpaces = myBoard->totalEmptySpaces;
		int *emptySpaces = myBoard->emptySpaces;
		int gridIndex = 0;
		int cand;

		while (gridIndex >= 0 && gridIndex < numberOfEmptySpaces && finished == 0) {
			cand = myBoard->GetNextCandidate(gridIndex);

			if (cand == 0) {
				if (myBoard->grid[emptySpaces[gridIndex]] != 0) {
					myBoard->grid[emptySpaces[gridIndex]] = 0;
					myBoard->RefreshEmptySpaceCandidates(emptySpaces[gridIndex]);
				}
				gridIndex--;
			} else {
				myBoard->grid[emptySpaces[gridIndex]] = cand;
				myBoard->RefreshEmptySpaceCandidates(emptySpaces[gridIndex]);
				gridIndex++;
			}
		}

		if (gridIndex == numberOfEmptySpaces && finished == 0) {
			if (atomicCAS(&finished, 0, 1) == 1)
				return;

			myBoard->Copy(solution);
		}

		index += stride;
	}
}

__host__ void DFSWrapper(int blocks, int blockThreads, GridArray *boards, Grid *solution) {
	int totalThreads = blocks * blockThreads;
	// DFS<<<blocks, blockThreads>>>(totalThreads, boards, solution);
	DFSMyron<<<blocks, blockThreads>>>(totalThreads, boards, solution);
	GPUAssert(cudaDeviceSynchronize());
	assert(solution->IsValid());
	assert(solution->IsComplete());
}

__global__ void DFSMyron(int totalThreads, GridArray *children, Grid *solution) {
	int threadIndex = blockDim.x * blockIdx.x + threadIdx.x;
	int stride = (totalThreads < blockDim.x * gridDim.x) ? totalThreads : blockDim.x * gridDim.x;
	for (int index = threadIndex; index < children->size(); index += stride) {
		// printf("index %d, chsize %d\n", index, children->size());
		Grid *myGrid = children->index(index);
		// printf("myGrid sizeTotal %d\n", myGrid->sizeTotal);
		Backtrack(myGrid, solution, 0);
		// printf("backtrack end\n");
	}
}

__device__ bool Backtrack(Grid *const &grid, Grid *const &solution, const int &index) {
	// printf("check0 index %d \n", index);
	assert(index <= grid->sizeTotal);
	// if (index == 12) printf("check1\n");
	if (finished) return true;	//other thread signals finished
	if (index == grid->sizeTotal) {
		// printf("sol\n");
		if (atomicCAS(&finished, 0, 1) == 1) return true;	//other thread was faster
		grid->Copy(solution);								 //we found the solution
		return true;
	}
	// if (index == 12) printf("check2\n");
	int i, j;
	grid->GetRowCol(&i, &j, index);
	// printf("index %d GotRowCol %d %d\n", index, i, j);
	if (grid->Get(i, j) > 0) {
		// if (index == 11) printf("index11 after get\n");
		return Backtrack(grid, solution, index + 1);	//next iteration, or propagate solution
	}

	bool *candidates = grid->GetCandidates(i, j);
	// printf("Got cands\n");
	for (int k = 1; k <= grid->sizeDim; ++k) {
		if (!candidates[k]) continue;
		grid->Put(i, j, k);
		if (Backtrack(grid, solution, index + 1)) {
			free(candidates);
			return true;	// propagate found solution
		};
		grid->Clear(i, j);
	}
	// printf("no cand\n");
	free(candidates);
	return false;	//no candidates, or all invalid eventually, roll back to try next candidate
}
