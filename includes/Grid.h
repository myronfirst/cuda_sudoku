#ifndef _GRID_H_
#define _GRID_H_

#include "Utilities.h"
#include <string>

struct Grid : public Managed {
	/* fields */
	int *grid;
	int totalEmptySpaces;
	int *emptySpaces;
	bool **emptySpacesCandidates;
	int size, sizeDim, sizeTotal; /* size: box row/col, sizeDim: grid row/col, sizeTotal: total cells */

	/* constructors/destructors */
	Grid(int _size);
	Grid(int _size, int value);
	~Grid();

	/* I/O */
	void Read(const std::string &fileName);
	CALLABLE void Print() const;

	/* candidate methods */
	//always call with 1 block, 3 threads
	CALLABLE bool *GetCandidates(const int &i, const int &j) const;
	CALLABLE void CreateEmptySpaces();
	CALLABLE void AllocateEmptySpacesCandidates();
	CALLABLE void RefreshEmptySpaceCandidates(int index);
	CALLABLE int GetNextCandidate(int index);

	/* rule methods */
	bool IsComplete();
	CALLABLE bool IsValid();

	/* primitive methods */
	CALLABLE void GetRowCol(int *const &i, int *const &j, const int &index) const;
	CALLABLE int Get(const int &i, const int &j) const;
	CALLABLE void Put(const int &i, const int &j, const int &value);
	CALLABLE void Clear(const int &i, const int &j);
	CALLABLE void Copy(Grid *dst) const;

	/* deprecated */
	bool IsHiddenSingle(const int &row, const int &col, const int &candidate) const;
};

#endif
