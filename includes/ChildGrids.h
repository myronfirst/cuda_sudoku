#ifndef _CHILDGRIDS_H_
#define _CHILDGRIDS_H_

#include "Grid.h"
#include "Utilities.h"

void ComputeChildGrids(int blocks, int blockThreads, int maxCells, const Grid *grid, SovietArray<Grid *> *childGrids);

#endif
