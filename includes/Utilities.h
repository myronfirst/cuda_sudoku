#ifndef _UTILITIES_H_
#define _UTILITIES_H_

// #include <cuda_runtime.h>
#include <iostream>
#include <cstdio>
#include <cassert>
#include <sys/time.h>

#ifdef __CUDACC__
#	define CALLABLE __host__ __device__
#else
#	define CALLABLE
#endif

#define GPUAssert(ans) \
	{ GpuAssert((ans), __FILE__, __LINE__); }
void GpuAssert(cudaError_t code, const char *file, int line, bool abort = true);

class Managed {
   public:
	void *operator new(size_t len);
	void operator delete(void *ptr);
};

template<class T>
class SovietArray : public Managed {
   private:
	T *arr;
	int alloc_len;
	int len;

   public:
	SovietArray(int _len) : len(_len), alloc_len(_len) {
		GPUAssert(cudaMallocManaged(&arr, alloc_len * sizeof(T)));
		GPUAssert(cudaMemset(arr, 0, alloc_len * sizeof(T)));
	}
	~SovietArray() {
		GPUAssert(cudaFree(arr));
	}
	CALLABLE T &index(const int &i) const {
		assert(i >= 0 && i < len);
		return arr[i];
	}
	CALLABLE int size() const {
		return len;
	}
	CALLABLE void size(int _len) {
		assert(_len >= 0 && _len <= alloc_len);
		len = _len;
	}
};

unsigned long GetTimeMillis(void);

#endif
