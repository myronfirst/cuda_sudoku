#ifndef _DFS_H_
#define _DFS_H_

#include "Utilities.h"
#include "Grid.h"

__host__ void DFSWrapper(int blocks, int blockThreads, SovietArray<Grid *> *boards, Grid *solution);

#endif