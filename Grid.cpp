#include "Grid.h"
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <cassert>
#include <sys/time.h>

using namespace std;
__global__ void threadCandidates(const Grid *grid, const int &row, const int &col, unsigned *candidates);

/* constructors/destructors */
Grid::Grid(int _size) : Grid(_size, 0){};

Grid::Grid(int _size, int value) : size(_size), sizeDim(_size * _size), sizeTotal(_size * _size * _size * _size) {
	assert(size > 0 && sizeDim > 0);
	GPUAssert(cudaMallocManaged(&grid, sizeTotal * sizeof(int)));
	//value < 0 acts as no memset flag, useful on Clone
	if (value >= 0) GPUAssert(cudaMemset(grid, value, sizeTotal * sizeof(int)));	//cudaMemset
}

Grid::~Grid() {
	GPUAssert(cudaFree(grid));
}

/* I/O */
void Grid::Read(const string &fileName) {
	ifstream file(fileName);
	if (!file.is_open()) {
		cout << "Check file path" << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < sizeTotal; ++i)
		file >> grid[i];
	file.close();
}

CALLABLE void Grid::Print() const {
	for (int i = 0; i < sizeDim; ++i) {
		for (int j = 0; j < sizeDim; ++j) {
			printf("%d ", Get(i, j));
		}
		printf("\n");
	}
}

/* candidate methods */
//always call with 1 block, 3 threads
CALLABLE bool *Grid::GetCandidates(const int &row, const int &col) const {
	assert(Get(row, col) == 0);
	bool *candidates = new bool[sizeDim + 1];
	memset(candidates, 1, (sizeDim + 1) * sizeof(bool));
	for (int i = 0; i < sizeDim; ++i) {
		int value = Get(i, col);
		if (value > 0 && candidates[value]) candidates[value] = false;
	}
	for (int i = 0; i < sizeDim; ++i) {
		int value = Get(row, i);
		if (value > 0 && candidates[value]) candidates[value] = false;
	}
	int boxRStart = (row / size) * size;
	int boxCStart = (col / size) * size;
	for (int i = boxRStart; i < boxRStart + size; ++i) {
		for (int j = boxCStart; j < boxCStart + size; ++j) {
			int value = Get(i, j);
			if (value > 0 && candidates[value]) candidates[value] = false;
		}
	}
	return candidates;
}

CALLABLE void Grid::CreateEmptySpaces() {
	for (int i = 0; i < sizeTotal; i++)
		if (grid[i] == 0)
			totalEmptySpaces++;

	emptySpaces = new int[totalEmptySpaces];
	memset(emptySpaces, 0, totalEmptySpaces * sizeof(int));

	int index = 0;
	for (int i = 0; i < sizeTotal; i++) {
		if (grid[i] == 0)
			emptySpaces[index++] = i;
	}
}

CALLABLE void Grid::AllocateEmptySpacesCandidates() {
	emptySpacesCandidates = new bool *[totalEmptySpaces];

	for (int i = 0; i < totalEmptySpaces; i++) {
		emptySpacesCandidates[i] = new bool[sizeDim + 1];
		memset(emptySpacesCandidates[i], true, (sizeDim + 1) * sizeof(bool));
	}

	for (int i = 0; i < totalEmptySpaces; i++) {
		int r = 0, c = 0;
		GetRowCol(&r, &c, emptySpaces[i]);

		for (int j = 0; j < sizeDim; j++) {
			int value = Get(j, c);
			if (value > 0)
				emptySpacesCandidates[i][value] = false;
		}
		for (int j = 0; j < sizeDim; j++) {
			int value = Get(r, j);
			if (value > 0)
				emptySpacesCandidates[i][value] = false;
		}
		int boxRStart = (r / size) * size;
		int boxCStart = (c / size) * size;
		for (int j = boxRStart; j < boxRStart + size; j++) {
			for (int k = boxCStart; k < boxCStart + size; k++) {
				int value = Get(j, k);
				if (value > 0)
					emptySpacesCandidates[i][value] = false;
			}
		}
	}
}

CALLABLE void Grid::RefreshEmptySpaceCandidates(int index) {
	for (int i = 0; i < totalEmptySpaces; i++) {
		int rI = 0, cI = 0;
		int rE = 0, cE = 0;

		GetRowCol(&rI, &cI, index);
		GetRowCol(&rE, &cE, emptySpaces[i]);
		/*
		if (rI != rE && cI != cE && (rE < (rI / size * size) || rE >= ((rI / size) * size + size)) && (cE < (cI / size) * size || cE >= (cI / size) * size + size))
			continue;
		*/
		memset(emptySpacesCandidates[i], true, (sizeDim + 1) * sizeof(bool));

		for (int j = 0; j < sizeDim; j++) {
			int value = Get(j, cE);
			if (value > 0)
				emptySpacesCandidates[i][value] = false;
		}
		for (int j = 0; j < sizeDim; j++) {
			int value = Get(rE, j);
			if (value > 0)
				emptySpacesCandidates[i][value] = false;
		}
		int boxRStart = (rE / size) * size;
		int boxCStart = (cE / size) * size;
		for (int j = boxRStart; j < boxRStart + size; j++) {
			for (int k = boxCStart; k < boxCStart + size; k++) {
				int value = Get(j, k);
				if (value > 0)
					emptySpacesCandidates[i][value] = false;
			}
		}
	}
}

CALLABLE int Grid::GetNextCandidate(int index) {
	for (int i = 1; i < sizeDim + 1; i++) {
		if (emptySpacesCandidates[index][i])
			return i;
	}

	return 0;
}

bool Grid::IsComplete() {
	for (int i = 0; i < sizeTotal; ++i)
		if (grid[i] == 0) return false;
	return true;
}

CALLABLE bool Grid::IsValid() {
	bool *set = new bool[sizeDim + 1]();	//set of seen value
	//rows
	for (int i = 0; i < sizeDim; ++i) {
		memset(set, false, (sizeDim + 1) * sizeof(bool));	//initialize set of seens to false
		for (int j = 0; j < sizeDim; ++j) {
			int val = Get(i, j);
			if (!(val >= 0 && val <= sizeDim)) return false;	//cell has valid value [0,sizeDim]
			if (val == 0) continue;								//cell is empty
			if (set[val] == true) return false;					//value already seen, invalid grid
			set[val] = true;									//value seen first time
		}
	}
	//cols
	for (int i = 0; i < sizeDim; ++i) {
		memset(set, false, (sizeDim + 1) * sizeof(bool));
		for (int j = 0; j < sizeDim; ++j) {
			int val = Get(j, i);
			if (!(val >= 0 && val <= sizeDim)) return false;
			if (val == 0) continue;
			if (set[val] == true) return false;
			set[val] = true;
		}
	}
	//boxes
	for (int index = 0; index < sizeTotal; index += sizeDim) {
		memset(set, false, (sizeDim + 1) * sizeof(bool));
		int row = index / sizeDim;
		int col = index % sizeDim;
		int boxRStart = (row / size) * size;
		int boxCStart = (col / size) * size;
		for (int i = boxRStart; i < boxRStart + size; ++i) {
			for (int j = boxCStart; j < boxCStart + size; ++j) {
				int val = Get(j, i);
				if (!(val >= 0 && val <= sizeDim)) return false;
				if (val == 0) continue;
				if (set[val] == true) return false;
				set[val] = true;
			}
		}
	}
	return true;
}

/* primitive methods */
CALLABLE void Grid::GetRowCol(int *const &i, int *const &j, const int &index) const {
	*i = index / sizeDim;
	*j = index % sizeDim;
}

CALLABLE int Grid::Get(const int &i, const int &j) const {
	return grid[i * sizeDim + j];
}

CALLABLE void Grid::Put(const int &i, const int &j, const int &value) {
	grid[i * sizeDim + j] = value;
}
CALLABLE void Grid::Clear(const int &i, const int &j) {
	grid[i * sizeDim + j] = 0;
}

CALLABLE void Grid::Copy(Grid *dst) const {
	memcpy(dst->grid, this->grid, this->sizeTotal * sizeof(int));
	dst->size = this->size;
	dst->sizeDim = this->sizeDim;
	dst->sizeTotal = this->sizeTotal;
}

/* deprecated */
bool Grid::IsHiddenSingle(const int &row, const int &col, const int &candidate) const {
	assert(Get(row, col) == 0);
	assert(candidate > 0);
	bool yes = true;
	for (int i = 0; i < sizeDim; ++i) {
		/* if (IsCandidate(i, col, candidate)) {
			yes = false;
			break;
		} */
	}
	if (!yes) return false;
	for (int i = 0; i < sizeDim; ++i) {
		/* if (IsCandidate(row, i, candidate)) {
			yes = false;
			break;
		} */
	}
	if (!yes) return false;
	int boxRStart = (row / size) * size;
	int boxCStart = (col / size) * size;
	for (int i = boxRStart; i < boxRStart + size; ++i) {
		for (int j = boxCStart; j < boxCStart + size; ++j) {
			/* if (IsCandidate(i, j, candidate)) {
				yes = false;
				break;
			} */
		}
	}
	if (!yes) return false;
	return true;
}

__global__ void threadCandidates(const Grid *grid, const int &row, const int &col, unsigned *candidates) {
	int threadIndex = threadIdx.x;
	printf("threadIndex %d\n", threadIndex);
	if (threadIndex == 0) {
		for (int i = 0; i < grid->sizeDim; ++i) {
			int value = grid->Get(i, col);
			if (value > 0 && candidates[value]) atomicCAS(&candidates[value], 1, 0);
		}
	}
	if (threadIndex == 1) {
		for (int i = 0; i < grid->sizeDim; ++i) {
			int value = grid->Get(row, i);
			if (value > 0 && candidates[value]) atomicCAS(&candidates[value], 1, 0);
		}
	}
	if (threadIndex == 2) {
		int boxRStart = (row / grid->size) * grid->size;
		int boxCStart = (col / grid->size) * grid->size;
		for (int i = boxRStart; i < boxRStart + grid->size; ++i) {
			for (int j = boxCStart; j < boxCStart + grid->size; ++j) {
				int value = grid->Get(i, j);
				if (value > 0 && candidates[value]) atomicCAS(&candidates[value], 1, 0);
			}
		}
	}
}
