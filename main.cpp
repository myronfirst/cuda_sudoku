#include "Grid.h"
#include "DFS.h"
#include "ChildGrids.h"
#include "Utilities.h"
#include <string>
#include <iostream>
#include <cassert>
#include <cstddef>

#define MAXCHILDREN 4096
#define GPUSTACK 256 * 1024

using namespace std;
using GridArray = SovietArray<Grid *>;
int main(int argc, char *argv[]) {
	//arguments
	if (argc != 6) {
		cout << "Usage: ./main size fileName maxCells childThreads DFSThreads" << endl;
		exit(EXIT_FAILURE);
	}
	int size = atoi(argv[1]);
	string fileName = string(argv[2]);
	int maxCells = atoi(argv[3]);
	int childThreads = atoi(argv[4]);
	int DFSThreads = atoi(argv[5]);
	if (size <= 0 || maxCells < 0 || childThreads <= 0 || DFSThreads <= 0) {
		cout << "Check input" << endl;
		exit(EXIT_FAILURE);
	}
	//memory
	Grid *input = new Grid(size);
	Grid *solution = new Grid(size);
	GridArray *childGrids = new GridArray(MAXCHILDREN);
	for (int i = 0; i < MAXCHILDREN; ++i) {
		childGrids->index(i) = new Grid(size);
	}
	size_t val;
	GPUAssert(cudaThreadGetLimit(&val, cudaLimitMallocHeapSize));
	cout << "GPU Heap " << val << endl;
	GPUAssert(cudaThreadSetLimit(cudaLimitStackSize, GPUSTACK));
	GPUAssert(cudaThreadGetLimit(&val, cudaLimitStackSize));
	cout << "GPU Stack " << val << endl;
	// return 0;

	input->Read(fileName);
	cout << "Input" << endl;
	input->Print();
	cout << endl;

	unsigned long tStart = GetTimeMillis();
	ComputeChildGrids(1, childThreads, maxCells, input, childGrids);
	for (int i = 0; i < childGrids->size(); ++i) {
		cout << "Child" << i << endl;
		assert(childGrids->index(i)->IsValid());
		childGrids->index(i)->Print();
		cout << endl;
	}
	DFSWrapper(1, DFSThreads, childGrids, solution);
	assert(solution->IsValid());
	assert(solution->IsComplete());
	unsigned long tEnd = GetTimeMillis();

	cout << "Solution" << endl;
	solution->Print();
	cout << endl;
	cout << tEnd - tStart << endl;
	return 0;
}
